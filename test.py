import json

import requests
import time

HEC_TOKEN = "Splunk 252006e9-ded1-4f72-a4a5-8a505c0a74aa"
headers = {'Authorization': HEC_TOKEN, 'Content-Type': 'application/json'}

def send_to_splunk(data):
    response = requests.post("https://prd-p-79ylm.splunkcloud.com:8088/services/collector/raw",headers=headers,json=data, verify=False)
    print(response.status_code)

def main():
    file = open("/var/lib/docker/containers/ed15683a3fba11a6d00a7e263ee1d77324f43b5552651994af05940db3833f71/ed15683a3fba11a6d00a7e263ee1d77324f43b5552651994af05940db3833f71-json.log",'r')
    while True:
        line=file.readline()
        if not line:
            break
        send_to_splunk(line)
        
if __name__ == "__main__":
    main()
